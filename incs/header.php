<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>EpiTV</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="css/style.css">
</head>

<body>

<nav class="navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
			<a href="?p=index" class="navbar-brand">EpiTV</a>
		</div>
		<ul class="nav navbar-nav navbar-right">
			<?php
			foreach ([
				 'rights' => "Gestion des droits",
				 'roles' => "Gestion des rôles",
				 'account' => "Gestion de compte",
				 'register' => "Inscription",
				 'login' => "Connexion",
			]
			as $link => $text)
			{
				if (!empty($_GET['p']) && $_GET['p'] == $link)
					echo '<li class="active"><a href="?p=' . $link . '">' . $text . '</a></li>';
				else
					echo '<li><a href="?p=' . $link . '">' . $text . '</a></li>';
			}
			?>
		</ul>
	</div>
</nav>
