<div class="container">
	<div class="page-header">
		<h3>Gestion de votre compte</h3>
	</div>

	<form action="" method="POST" class="form-horizontal">
		<fieldset>
			<legend>
				Informations
			</legend>

			<div class="form-group">
				<label for="inputLogin" class="col-lg-2 control-label">Login (non modifiable)</label>
				<div class="col-lg-10">
					<input type="text" class="form-control" id="inputLogin" disabled value="login_a">
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail" class="col-lg-2 control-label">Adresse e-mail (non modifiable)</label>
				<div class="col-lg-10">
					<input type="text" class="form-control" id="inputEmail" disabled value="login_a@epitech.eu">
				</div>
			</div>
			<div class="form-group">
				<label for="inputFirstname" class="col-lg-2 control-label">Prénom</label>
				<div class="col-lg-10">
					<input type="text" class="form-control" id="inputFirstname" placeholder="">
				</div>
			</div>
			<div class="form-group">
				<label for="inputLastname" class="col-lg-2 control-label">Nom</label>
				<div class="col-lg-10">
					<input type="text" class="form-control" id="inputLastname" placeholder="">
				</div>
			</div>
			<div class="form-group">
				<div class="col-lg-10 col-lg-offset-2">
					<button type="submit" class="btn btn-primary">Enregistrer</button>
				</div>
			</div>
		</fieldset>
	</form>

	<form action="" method="POST" class="form-horizontal">
		<fieldset>
			<legend>
				Modification du mot de passe
			</legend>

			<div class="form-group">
				<label for="inputPassword" class="col-lg-2 control-label">Nouveau mot de passe</label>
				<div class="col-lg-10">
					<input type="password" class="form-control" id="inputPassword" placeholder="">
				</div>
			</div>
			<div class="form-group">
				<label for="inputConfirmation" class="col-lg-2 control-label">Confirmation du mot de passe</label>
				<div class="col-lg-10">
					<input type="password" class="form-control" id="inputConfirmation" placeholder="">
				</div>
			</div>
			<div class="form-group">
				<div class="col-lg-10 col-lg-offset-2">
					<button type="submit" class="btn btn-primary">Enregistrer</button>
				</div>
			</div>
		</fieldset>
	</form>

</div>
