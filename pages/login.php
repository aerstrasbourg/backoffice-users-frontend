<div class="container">
	<div class="page-header">
		<h3>Connexion</h3>
	</div>

	<div class="well">
		<form class="form-horizontal">
			<fieldset>

				<div class="form-group">
					<label for="inputLogin" class="col-lg-2 control-label">Login Epitech</label>
					<div class="col-lg-10">
						<input type="text" class="form-control" id="inputLogin" placeholder="login_a">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword" class="col-lg-2 control-label">Mot de passe</label>
					<div class="col-lg-10">
						<input type="password" class="form-control" id="inputPassword" placeholder="">
					</div>
				</div>

				<div class="form-group">
					<div class="col-lg-10 col-lg-offset-2">
						<button type="reset" class="btn btn-default">Annuler</button>
						<button type="submit" class="btn btn-primary">Connexion</button>
					</div>
				</div>

			</fieldset>
		</form>
	</div>
</div>
