<div class="container">
	<div class="page-header">
		<h3>Inscription</h3>
	</div>

	<div class="well">
		<form action="" method="POST" class="form-horizontal">
			<fieldset>

				<div class="form-group">
					<label for="inputLogin" class="col-lg-2 control-label">Login Epitech</label>
					<div class="col-lg-10">
						<input type="text" class="form-control" id="inputLogin" placeholder="login_a">
						<span class="help-block">Un e-mail d'activation sera envoyé à votre adresse Epitech <em>login_a@epitech.eu</em></span>
					</div>
				</div>
				<div class="form-group has-error">
					<label for="inputPassword" class="col-lg-2 control-label">Mot de passe</label>
					<div class="col-lg-10">
						<input type="password" class="form-control" id="inputPassword" placeholder="">
						<span class="help-block">Votre mot de passe doit faire au moins 42 caractères.</span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputConfirmation" class="col-lg-2 control-label">Confirmation du mot de passe</label>
					<div class="col-lg-10">
						<input type="password" class="form-control" id="inputConfirmation" placeholder="">
					</div>
				</div>

				<div class="form-group">
					<div class="col-lg-10 col-lg-offset-2">
						<button type="reset" class="btn btn-default">Annuler</button>
						<button type="submit" class="btn btn-primary">Inscription</button>
					</div>
				</div>

			</fieldset>
		</form>
	</div>
</div>
