<div class="container">
	<div class="page-header">
		<h3>Gestion des droits</h3>
	</div>

	<form action="" method="POST" class="form-horizontal">

		<fieldset>
			<legend>
				Création d'une nouvelle règle
			</legend>

			<div class="input-group">
				<input type="text" id="inputRule" class="form-control" placeholder="Valeur de la nouvelle règle">
				<span class="input-group-btn">
					<button class="btn btn-primary" type="button">Enregistrer</button>
				</span>
			</div>
		</fieldset>

	</form>

	<p class="lead">Modification des droits pour le rôle <em>Administration</em></p>

	<form action="" method="POST" class="form-horizontal">

		<fieldset>
			<legend>
				Utilisateurs
			</legend>

			<?php $max = rand(7, 25); for ($i = 0; $i < $max; $i++) { ?>
			<div class="form-group checkbox col-md-3 col-sm-4 col-xs-12">
				<label>
					<input type="checkbox" <?= (rand(0, 1) == 0 ? 'checked="checked"' : '') ?> /> Description de la checkbox
				</label>
			</div>
			<?php } ?>
		</fieldset>

		<fieldset>
			<legend>
				Compositions
			</legend>

			<?php $max = rand(7, 25); for ($i = 0; $i < $max; $i++) { ?>
			<div class="form-group checkbox col-md-3 col-sm-4 col-xs-12">
				<label>
					<input type="checkbox" <?= (rand(0, 1) == 0 ? 'checked="checked"' : '') ?> /> Description de la checkbox
				</label>
			</div>
			<?php } ?>
		</fieldset>

		<fieldset>
			<legend>
				Administration
			</legend>

			<?php $max = rand(7, 25); for ($i = 0; $i < $max; $i++) { ?>
			<div class="form-group checkbox col-md-3 col-sm-4 col-xs-12">
				<label>
					<input type="checkbox" <?= (rand(0, 1) == 0 ? 'checked="checked"' : '') ?> /> Description de la checkbox
				</label>
			</div>
			<?php } ?>
		</fieldset>

		<fieldset>
			<button type="submit"  class="btn btn-primary">Enregistrer</button>
		</fieldset>

	</form>

</div>