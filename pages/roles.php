<div class="container">
	<div class="page-header">
		<h3>Gestion des rôles</h3>
	</div>

	<form action="" method="POST" class="form-horizontal">

		<fieldset>
			<legend>
				Création d'un nouveau rôle
			</legend>

			<div class="input-group">
				<input type="text" id="inputRole" class="form-control" placeholder="Nom du nouveau rôle">
				<span class="input-group-btn">
					<button class="btn btn-primary" type="button">Enregistrer</button>
				</span>
			</div>
		</fieldset>

	</form>

	<?php
	foreach ([
		 'Administration',
		 'Pédagos',
		 'AER',
		 'Etudiants',
	] as $role) {
	?>
	<form action="" method="POST" class="form-horizontal">

		<fieldset>
			<legend>
				Rôle <em><?= $role; ?></em>
				<span class="pull-right small"><a href="?p=rights">Gestion des droits</a></span>
			</legend>

			<table class="table table-hover table-striped no-bottom-margin">
				<?php $max = rand(1, 5); for ($i = 0; $i < $max; $i++) { ?>
				<tr>
					<td class="col-md-8">login_a</td>
					<td class="col-md-2 text-right"><a href="#">Modifier l'utilisateur</a></td>
					<td class="col-md-2 text-right"><a href="#">Enlever le rôle</a></td>
				</tr>
				<?php } ?>
			</table>
			<div class="input-group">
				<input type="text" id="inputRole" class="form-control" placeholder="Ajouter un utilisateur">
				<span class="input-group-btn">
					<button class="btn btn-primary" type="button">Ajouter</button>
				</span>
			</div>
		</fieldset>

	</form>
	<?php } ?>

</div>